import os
import numpy
import datetime
import subprocess

def get_cosima_source(path):
	ALL = os.listdir(path)
	sims = []
	for i,item in enumerate(ALL):
		if item.endswith('.source'):
			sims.append(item)

	question = ''
	for i,ele in enumerate(sims):
		question+=str(i)+' ' + ele + '\n'

	k = int(input(question + 'Enter your choice:'))
	return sims[k]

###########################################################

def get_geomega_source(path):
	folders = os.listdir(path)
	geos = []

	for fol in folders:
		if not fol.endswith('.md'):
			ALL = os.listdir(path+fol)
			for i,item in enumerate(ALL):
				if item.endswith('.geo.setup'):
					geos.append(fol+'/'+item)

	question = ''
	for i,ele in enumerate(geos):
		question+=str(i)+' ' + ele + '\n'

	k = int(input(question + 'Enter your choice:'))
	return geos[k]

###########################################################

def run(*popenargs, **kwargs):
    input = kwargs.pop("input", None)
    check = kwargs.pop("handle", False)

    if input is not None:
        if 'stdin' in kwargs:
            raise ValueError('stdin and input arguments may not both be used.')
        kwargs['stdin'] = subprocess.PIPE

    process = subprocess.Popen(*popenargs, **kwargs)
    try:
        stdout, stderr = process.communicate(input)
    except:
        process.kill()
        process.wait()
        raise
    retcode = process.poll()
    if check and retcode:
        raise subprocess.CalledProcessError(
            retcode, process.args, output=stdout, stderr=stderr)
    return retcode, stdout, stderr

###########################################################

def sourceFileMaker(path, OneBeam, myene, cosTh, ang):
    with open(path,'r') as f:
        data = f.read()
        
        OBI  = data.index('.FileName')
        insert = '{}_{:.3f}MeV_Cos{:.1f}'
        endline = data[OBI+10+len(insert):].index('\n')
        data = data[:OBI+10]+ insert + data[OBI+10+len(insert)+endline:]
        
        OBI = data.index('.Beam')
        data = data[:OBI+10]+ '{}  {:.1f} 0     //' + data[OBI+10:]
        
        OBI  = data.index(' Mono')
        data = data[:OBI+6]+ '{}  //' + data[OBI+6:]
    
    ret = data.format(OneBeam, myene/1000., cosTh, OneBeam, ang, myene)
    return ret
