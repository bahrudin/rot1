import os
import numpy
import datetime
from helpers import *
import math
import time

#path to the folder where things will happen
path   = '/nfs/farm/g/superb/u01/lz/MeV/users/bahrudin/rot1/'

#paths to the geometry and sources
cosima_file  = "/nfs/farm/g/superb/u01/lz/MeV/users/bahrudin/megalibgeo/sources/"
sourcefilepath= cosima_file + "TestBeam.source"
sourcefilepath= path + "TestBeamShort.source" ### DELETE THIS. Uset for test purposes only - short test duration t=100s

#if you wanna use the geometry defined in the .source file
default_geometry = True 
geofolder = "/nfs/farm/g/superb/u01/lz/MeV/users/bahrudin/megalibgeo/geometries/"
geofile   = geofolder + "ComptonTelescopeHexCells.geo.setup"

OneBeam = 'FarFieldPointSource'

#config files
revan_config  = "/nfs/farm/g/superb/u01/lz/MeV/users/bahrudin/megalibgeo/configs/revan_driftChamber_settings.cfg"
mimrec_config = "/nfs/farm/g/superb/u01/lz/MeV/users/bahrudin/megalibgeo/configs/mimrec_driftChamber_settings.cfg"

#define your energies and angles 
Log_E  = [2.2,3,5.5,6.7]
angles = [0,45.6,53.1,60]

############################################
#### From now on, the script takes over #### 
############################################

#change the execution enviroment to path
os.chdir(path)
curdir = os.getcwd()

#create new folder named RUN+timestamp
cur_time = datetime.datetime.now().strftime("%H%M%S")
new_name = "RUN" + cur_time
os.mkdir(path + new_name)
os.chdir(path + new_name)

curdir = os.getcwd()
print("Current directory is: "+ str(curdir))

############################################
# generate sourcefiles with angles and energies 
############################################

#the prep cosima script
energies = [int(10**ee) for ee in Log_E]
cos_ang  = [round(math.cos(math.radians(i)),1) for i in angles]


source_FILES = []
for myene in energies:
    for cosTh,ang in zip(cos_ang,angles):
        dat = sourceFileMaker(sourcefilepath, OneBeam, myene, cosTh, ang)
        
        source_file='%s_%.3fMeV_Cos%.1f.source'%(OneBeam,myene/1000.,cosTh)
        sf=open(source_file,'w')
        sf.write(dat)
        sf.close()
        source_FILES.append(source_file)

#at this point we have a bunch of .source files in our working directory
#now we need to process them through cosima so we
#submit the jobs to SLAC via bsub ''''' bsub -R "centos7" -W 3600 -o output.log yourcommand your_options

# run cosima
for cosFile in source_FILES:
    logname = cosFile[:-7]+'.log'
    run(['bsub','-R','"centos7"', '-W' ,'3600','-o' ,logname, 'cosima',cosFile]) 


simfile = os.listdir(curdir)[0] # TO DOOOOO

if default_geometry:
    with open(simfile) as f:
        for s in f.readlines():
            if "Geometry" in s:
                geomegaSRC = s.strip("Geometry   ")
                geomegaSRC = str(geomegaSRC.strip())


if not default_geometry:
	geomegaSRC = geomega_sources + get_geomega_source(geomega_sources)

expected_outputs = len(Log_E)*len(angles)

# get all items in the output RUN#### folder
out_files = os.listdir()
print out_files

log_files = [lf for lf in out_files if lf.endswith(".log")]
sim_files = [lf for lf in out_files if lf.endswith(".sim")]

# Exit status in .log files: Successfully completed. OR 
# first wait to recieve all Successfully completed or failed
all_done = False 

while not all_done:
    completed=0
    list_of_failed = []
    for lf in log_files:
        with open(lf) as f:
            s = f.read()
            if "Successfully completed" in s: 
                completed+=1
            elif "There was a crash" in s:
                completed+=1
                list_of_failed.append(lf)
    
    if completed == expected_outputs:
        all_done = True

for cosFile in list_of_failed:
    logname = cosFile[:-4]+'2.log'
    run(['bsub','-R','"centos7"', '-W' ,'3600','-o' ,logname, 'cosima',cosFile[:-4]+".source"]) 



# #run revan
# RUNrev = run(['revan',"-f",simfile,"-g",geomegaSRC,"-c",revan_config,'-a',"-n"])
# print(RUNrev)




