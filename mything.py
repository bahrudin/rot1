import os
import numpy
import datetime
from helpers import *

path   = '/nfs/farm/g/superb/u01/lz/MeV/users/bahrudin/rot1/'
default_geometry = False

cosima_examples = "/nfs/farm/g/superb/u01/lz/MeV/users/bahrudin/megalibgeo/sources/"
geomega_sources = "/nfs/farm/g/superb/u01/lz/MeV/users/bahrudin/megalibgeo/"

revan_config    = "/nfs/farm/g/superb/u01/lz/MeV/users/bahrudin/megalibgeo/configs/revan_driftChamber_settings.cfg"
mimrec_config   = "/nfs/farm/g/superb/u01/lz/MeV/users/bahrudin/megalibgeo/configs/mimrec_driftChamber_settings.cfg"

os.chdir(path)
curdir = os.getcwd()

cur_time = datetime.datetime.now().strftime("%H%M%S")
new_name = "RUN" + cur_time

os.mkdir(path + new_name)
os.chdir(path + new_name)

curdir = os.getcwd()
print("Current directory is:",curdir,'\n--\n')

cosimaSRC = cosima_examples + get_cosima_source(cosima_examples)

# run cosima
RUNcos = run(['cosima',cosimaSRC])
print(RUNcos)

simfile = os.listdir(curdir)[0]

if default_geometry:
	with open(simfile) as f:
		s = f.readlines()[5]
		geomegaSRC = s.strip("Geometry   ")
		geomegaSRC = str(geomegaSRC.strip())

else:
	geomegaSRC = geomega_sources + get_geomega_source(geomega_sources)


#run revan
RUNrev = run(['revan',"-f",simfile,"-g",geomegaSRC,"-c",revan_config,'-a',"-n"])
print(RUNrev)


#run mimrec
trafile = simfile.replace('sim','tra')
RUNmim  = run(['mimrec',"-f",trafile,"-g",geomegaSRC,"-c",mimrec_config])
print(RUNmim)
